<?php 
$sourceFile = 'source.gif';
$destionationFile = 'destination.gif';
$watermarkFile = 'watermark.png';

$image = new Imagick($sourceFile); 
$watermark = new Imagick($watermarkFile);

$iWidth = $image->getImageWidth();
$iHeight = $image->getImageHeight();

$wWidth = $watermark->getImageWidth();
$wHeight = $watermark->getImageHeight();

if ($iHeight < $wHeight || $iWidth < $wWidth) {
    $watermark->scaleImage($iWidth, $iHeight);
    $wWidth = $watermark->getImageWidth();
    $wHeight = $watermark->getImageHeight();
}
$x = ($iWidth - $wWidth) / 2;
$y = ($iHeight - $wHeight) / 2;

$image = $image->coalesceImages(); 
foreach ($image as $frame) { 
    $frame->compositeImage($watermark, imagick::COMPOSITE_OVER, $x, $y);
} 
$image = $image->deconstructImages(); 
$image->writeImages($destionationFile, true); 
?>

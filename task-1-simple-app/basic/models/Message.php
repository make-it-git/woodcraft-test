<?php

namespace app\models;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use Yii;

class Message extends ActiveRecord
{
    public function rules()
    {
        return [
            ['message', 'required'],
            ['message', 'string'],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => null
            ],
            [
                'class' => TimestampBehavior::className(),
            ]
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

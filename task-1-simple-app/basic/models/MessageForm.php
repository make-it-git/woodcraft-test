<?php

namespace app\models;
 
use yii\base\Model;
use Yii;
 
class MessageForm extends Model
{
    public $message;
 
    public function rules()
    {
        return [
            ['message', 'filter', 'filter' => 'trim'],
            ['message', 'required'],
            ['message', 'string', 'min' => 3, 'max' => 256],
        ];
    }
 
    /**
     * Send message
     *
     * @return User|null the saved model or null if saving fails
     */
    public function send()
    {
        if ($this->validate()) {
            $message = new Message();
            $message->message = $this->message;
            if ($message->save()) {
                return $message;
            }
        }
 
        return null;
    }
}

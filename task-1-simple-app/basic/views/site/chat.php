<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Чат';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php
        foreach ($messages as $message) {
    ?>
        <div class="row col-sm-12">
            <div class="col-sm-6 col-xs-6">
                <?php echo Html::encode($message->message) ?>
            </div>
            <div class="col-sm-3 badge badge-info">
                <?php echo Html::encode($message->user->email) ?>
            </div>
            <div class="col-sm-3">
                <?php echo Yii::$app->formatter->asTime($message->created_at) ?>
            </div>
        </div>
    <?php
        }
    ?>

    <?php $form = ActiveForm::begin([
        'id' => 'chat-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'message')->textInput(['autofocus' => true]) ?>
        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-6">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>


</div>

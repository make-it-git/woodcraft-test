"use strict";
(function ( $ ) {

    $.fn.follow = function(options) {
        var $selectedNode = null;
        var $document = $(document);

        var settings = $.extend({
              position: 'center'
        }, options);

        $document.on('mousemove', function(event) {
            if ($selectedNode === null) return;
            var x = event.pageX;
            var y = event.pageY;
            $selectedNode
                .css('position', 'absolute')
                .css('z-index', 9999);
            switch (settings.position) {
                case 'top-left':
                    // Do not change x,y as it is already top left
                    break;
                case 'center':
                    var width = $selectedNode.width();
                    var height = $selectedNode.height();
                    x = x - width / 2;
                    y = y - height / 2;
                    break;
            }
            $selectedNode
                .css('left', x + 'px')
                .css('top', y + 'px')
        });

        $document.on('click', function() {
            $selectedNode = null;
        });

        var handleMouseOver = function(event) {
            var node = event.target;
            if ($selectedNode === null) {
                $selectedNode = $(node);
            }
        };

        this.each(function(index, item) {
            var $item = $(item);
            $item.on('mouseover', handleMouseOver);
        });
    };
}( jQuery ));
